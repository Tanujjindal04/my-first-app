import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-servers',
  templateUrl: './servers.component.html',
  styleUrls: ['./servers.component.css']
})
export class ServersComponent implements OnInit {
  allowNewServers = false;
  serverCreationStatus = "No server was created";
  servername = ' Default Value ';
  constructor() {
    setTimeout(()=>{
      this.allowNewServers=true;
    },2000);
   }

  ngOnInit() {
  }

  onCreateServer(){
    this.serverCreationStatus = "Servers was created ! New Server Name : " + this.servername;
  }

  updateServerName(event: Event){
    this.servername = (<HTMLInputElement>event.target).value;
  }

}
